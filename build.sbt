scalariformSettings

organization := "co.s4n"

name := "kafka-example"

scalaVersion := "2.12.1"

resolvers ++= Seq(
  "releases" at "http://oss.sonatype.org/content/repositories/releases"
)

val circeVersion = "0.7.0"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies ++= {

  val akkaHttpV = "10.0.4"
  val phantomV = "2.1.3"
  val akkaV = "2.4.17"

  Seq(

    "com.iheart"                  %%  "ficus"                     % "1.4.0",
    "ch.qos.logback"              %   "logback-classic"           % "1.1.7",
    "com.typesafe.scala-logging"  %%  "scala-logging"             % "3.5.0",
    "org.apache.kafka" 	          %   "kafka-clients"		          % "0.10.2.0",
    "org.scalatest"               %   "scalatest_2.12"            % "3.0.0"     % "test"
  )
}

coverageEnabled := true

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Xcheckinit"
)

publishMavenStyle := true

pomIncludeRepository := { _ => false }

publishArtifact in Test := false

publishTo := {
  val nexus = "http://somewhere/nexus/"
  if (version.value.trim.endsWith("SNAPSHOT"))
    Some("Nexus Snapshots" at nexus + "content/repositories/snapshots/")
  else
    Some("Nexus Releases" at nexus + "content/repositories/releases")
}

credentials += Credentials("Sonatype Nexus Repository Manager", "somewhere", "user", "password")
