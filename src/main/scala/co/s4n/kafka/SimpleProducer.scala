package co.s4n.kafka

import java.util.Properties

import com.typesafe.scalalogging.LazyLogging
import org.apache.kafka.clients.producer._

class SimpleProducer(broker: String) extends LazyLogging {

  private val ProducerProperties: Properties = {
    val properties: Properties = new Properties()
    // Kafka Server
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, broker)
    // Use a byte[] as a Key
    properties.put(
      ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
      // "org.apache.kafka.common.serialization.StringSerializer"
      "org.apache.kafka.common.serialization.ByteArraySerializer"
    )
    // Use a string as a value
    properties.put(
      ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringSerializer"
    )
    properties
  }

  private val producer: KafkaProducer[String, String] =
    new KafkaProducer[String, String](ProducerProperties)

  def sendToTopic(topic: String, message: String): Unit = {
    val record: ProducerRecord[String, String] =
      new ProducerRecord[String, String](topic, message)
    producer.send(record, (k, v) => {})
    logger.info("Sending {} to  topic {}", message, topic)
  }

  def stop(): Unit = producer.close()
}
object SimpleProducer extends App {

  import util.control.Breaks._
  import scala.io.StdIn

  val producer = new SimpleProducer("127.0.0.1:9092")

  breakable{
      for (x <- 0 to 1000){
        val msg: String = StdIn.readLine()
        if(msg.equalsIgnoreCase("exit")) {
          break
        }
        else producer.sendToTopic(topic = "test", message = msg)
    }
  }
  producer.stop()
}