package co.s4n.kafka

import java.util.concurrent.Executors
import java.util.{ Collections, Properties }

import com.typesafe.scalalogging.LazyLogging
import org.apache.kafka.clients.consumer._

import scala.concurrent.{ ExecutionContext, ExecutionContextExecutorService, Future }

class SimpleConsumer(broker: String, groupId: String, topic: String) extends LazyLogging {

  private val ConsumerProperties: Properties = {
    val properties: Properties = new Properties()
    properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, broker)
    properties.put(
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
      // "org.apache.kafka.common.serialization.StringDeserializer"
      "org.apache.kafka.common.serialization.ByteArrayDeserializer"
    )
    properties.put(
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringDeserializer"
    )
    properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId)
    properties.put(ConsumerConfig.CLIENT_ID_CONFIG, "simple")
    properties
  }

  private val KafkaConsumer: KafkaConsumer[String, String] =
    new KafkaConsumer[String, String](ConsumerProperties)

  // private val executor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

  implicit val executionContext: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(Executors.newSingleThreadScheduledExecutor())

  def stop(): Unit = {
    KafkaConsumer.close()
    executionContext.shutdown()
    logger.info("Stopped!")
  }

  def run: Future[Unit] = Future {
    KafkaConsumer.subscribe(Collections.singletonList(topic))
    while (true) {
      val records: ConsumerRecords[String, String] = KafkaConsumer.poll(100)
      records.forEach(record => logger.info(s"[${record.key()}, ${record.value()}]"))
    }
  }
}
object SimpleConsumerApp extends App{

  val consumer = new SimpleConsumer("127.0.0.1:9092", "group-1", "test")
  consumer.run
}
